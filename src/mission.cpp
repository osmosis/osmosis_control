//juil2018 J.Guiochet @ LAAS
//juil2019 Anthony FAVIER / Antonin MESSIOUX @ LAAS
#include <osmosis_control/mission.hpp>

namespace osmosis {

MissionManager::MissionManager()
{
	state = IDLE;
	mission_state = INIT_MISSION;
}

std::string MissionManager::get_state_str() const {
  std::string state_str, task_str;
  switch (state) {
    case IDLE:
      state_str = "IDLE";
      break;
    case REACH_POINT_MISSION:
      state_str = "REACH_POINT_MISSION";
      break;
    case RUNWAY_MISSION:
      state_str = "RUNWAY_MISSION";
      break;
    case MISSION_DONE:
      state_str = "MISSION_DONE";
      break;
  }
  switch (mission_state) {
    case INIT_MISSION:
      task_str = "INIT_MISSION";
      break;
    case SEND_MISSION:
      task_str = "SEND_MISSION";
      break;
    case EXECUTE_MISSION:
      task_str = "EXECUTE_MISSION";
      break;
  }
  return state_str + "/" + task_str;
}

MissionManager::State MissionManager::get_state() const {
  return state;
}
MissionManager::StateTask MissionManager::get_state_task() const {
  return mission_state;
}

void MissionManager::step(bool& mission_received,
  bool& goal_reached,
  const osmosis_control_msgs::Mission mission,
  std_msgs::Bool& done_msg,
  osmosis_control_msgs::Goal& goal_msg,
  std::string mission_folder)
{
	switch (state) {
		case IDLE:
			if (mission_received) {
				mission_received=false;
				if (mission.doRunwayMission)
					state = RUNWAY_MISSION;
				else
					state = REACH_POINT_MISSION;
			}
			break;

		case REACH_POINT_MISSION:
			switch (mission_state) {
				case INIT_MISSION:
					goalKeyboard(mission, goal_msg);
          goal_reached=false;
					mission_state = SEND_MISSION;
					break;
				case SEND_MISSION:
					mission_state = EXECUTE_MISSION;
					break;
				case EXECUTE_MISSION:
					if(goal_reached) {
						setDone(done_msg);
						mission_state = INIT_MISSION;
						state = MISSION_DONE;
					}
					break;
			}
			break;

		case RUNWAY_MISSION:
			switch (mission_state) {
				case INIT_MISSION:
					initMission(mission_folder, mission.mission_name);
          goal_reached = false;
					mission_state = SEND_MISSION;
					break;
        case SEND_MISSION:
          mission_state = EXECUTE_MISSION;
				case EXECUTE_MISSION:
					if (checkNextStep(goal_reached, goal_msg))
						mission_state = SEND_MISSION;

					if (isMissionOver())
					{
						setDone(done_msg);
						mission_state = INIT_MISSION;
						state = MISSION_DONE;
					}

					break;
			}
			break;

    case MISSION_DONE:
        state = IDLE;
			break;

		default: break;
	}
}

void MissionManager::goalKeyboard(const osmosis_control_msgs::Mission& mission,
  osmosis_control_msgs::Goal& goal)
{
	goal = mission.mission_goal;
}

void MissionManager::setDone(std_msgs::Bool& done)
{
	done.data=true;
}

void MissionManager::initMission(std::string folder, std::string name)
{
	steps.clear();
	std::string filename = folder + "/MISSION_" + name + ".miss";
	std::ifstream fichier(filename, std::ios::in);
	std::string line;
	while(getline(fichier, line))
		parse(line);
	std::cout << "Mission with " << steps.size() << " steps" << std::endl;
	fichier.close();
	/*for(i=0; i<mission_.mission_steps.size();i++)
	{
		ROS_INFO("x: %f",mission_.mission_steps[i].point.x);
		ROS_INFO("y: %f",mission_.mission_steps[i].point.y);
		ROS_INFO("taxi= %d",mission_.mission_steps[i].taxi);
	}*/
  mission_step=0;
}

void MissionManager::parse(std::string line)
{
	osmosis_control_msgs::Goal step;

	if(line.size()>=14)
	{
		int coma=line.find(',');
		if(coma>=0)
		{
			int coma2=line.find(',', coma+1);
			if(coma2>=0)
			{
				step.point.x=stof(line.substr(2, coma-2));
				step.point.y=stof(line.substr(coma+3, coma2-coma-3));
				step.taxi=stoi(line.substr(coma2+6,1))!=0;

				steps.push_back(step);
			}
		}
	}
}

bool MissionManager::checkNextStep(bool& goal_reached, osmosis_control_msgs::Goal& goal)
{
	bool next=false;
	if (goal_reached) {
		mission_step++;
		if (! isMissionOver())
		{
			next = true;
			nextStep(goal_reached, goal);
		}
	}
	return next;
}

bool MissionManager::isMissionOver()
{
	bool over=false;
	if(mission_step == steps.size())
		over=true;
	return over;
}

void MissionManager::nextStep(bool& goal_reached, osmosis_control_msgs::Goal& goal)
{
	goal_reached = false;
	goal = steps[mission_step];
}

}; // ns
