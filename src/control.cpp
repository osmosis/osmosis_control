/*
* Copyright 2018 LAAS-CNRS
*
* This file is part of the OSMOSIS project.
*
* Osmosis is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Osmosis is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
*/

#include <osmosis_control/control.hpp>

namespace osmosis {

template <typename T> int sign(T val) {
 return (T(0) < val) - (val < T(0));
};

Control::Control() {
  state = State::WAIT_GOAL;
  target.x = std::numeric_limits<double>::infinity();
  target.y = std::numeric_limits<double>::infinity();
}

//Potential Field Algo
geometry_msgs::Twist Control::PF(double x_p, double y_p,double theta_p, double obs_dx, double obs_dy,
  const double nu, const double psi, const double safety_distance)
{
	geometry_msgs::Twist control;

	//Collision Avoidance gains and parameters
	double Fatt1=0, Fatt2=0;
	double Frep1=0, Frep2=0;
	double F1, F2;
	double epsilon = 0.11;
	double V1=0, V2=0;
	double alpharep=4;
	double umax = 1.0;
	double wmax = 1.0;

	if (theta_p < -M_PI)
		theta_p = theta_p + 2*M_PI;
	else if (theta_p >M_PI)
		theta_p = theta_p - 2*M_PI;

	double dist_g = sqrt(pow(x_p,2)+pow(y_p,2));

	double dist = sqrt(pow((x_p-obs_dx),2)+pow((y_p-obs_dy),2));

	///////// attractive force
	if (dist_g <= nu)
	{
		Fatt1 = -2*x_p;
		Fatt2 = -2*y_p;
	}
	if (dist_g > psi)
	{
		Fatt1 = -x_p/dist_g;
		Fatt2 = -y_p/dist_g;
	}

	///////// repulsive force
	if (dist < safety_distance)
	{
		double value = pow(safety_distance,2) - pow(dist,2);
		Frep1 = 4 * alpharep*fmax(0,value)*(x_p-obs_dx);
		Frep2 = 4*alpharep*fmax(0,value)*(y_p-obs_dy);
	}
	///////// composition
	F1 = Fatt1 +Frep1;
	F2 = Fatt2 +Frep2;
	///////// Eventual V
	if (sqrt(pow(F1,2)+pow(F2,2)) <= epsilon)
	{
		double rho = y_p - (obs_dy/obs_dx) * x_p;
		int s = sign(rho);
		V1 = ( (s*epsilon) / dist_g ) * y_p;
		V2 = ( (s*epsilon) / dist_g ) * (-x_p);
	}
	/////////
	F1 = Fatt1 + Frep1 - V1;
	F2 = Fatt2 + Frep2 - V2;

	double theta_d = atan2(F2,F1);
	double err = theta_d - theta_p;
	if (err < -M_PI)
		err = err + 2*M_PI;
	else if (err > M_PI)
		err = err - 2*M_PI;
	int s = sign(err);

	control.angular.z = wmax * sqrt(fabs(err)) * s;
	control.linear.x = (umax / (1+epsilon)) * (sqrt(pow(F1,2)+pow(F2,2)));

	return control;
}

bool Control::obstacleFromScan(const sensor_msgs::LaserScan& scan,
  geometry_msgs::Pose2D& obstacle,
  const double obstacle_distance)
{
	bool obs=false;

	double xmin = std::numeric_limits<double>::max();
	double xmax = std::numeric_limits<double>::min();
	double ymin = std::numeric_limits<double>::max();
	double ymax = std::numeric_limits<double>::min();
	double far = obstacle_distance;

	for (int i = 0; i < scan.ranges.size(); i++)
	{
		//if (scan.ranges[i] < scan.range_min) continue; //avoid use of continue see below
		if (scan.ranges[i] >= scan.range_min)
		{
			if (scan.ranges[i] <= far)
			{
				obs=true;

				double dist = scan.ranges[i];
				double ang = scan.angle_min + (scan.angle_max - scan.angle_min) * i / scan.ranges.size();
				double px = dist * cos(ang);
				double py = dist * sin(ang);
				if (px < xmin) xmin = px;
				if (px > xmax) xmax = px;
				if (py < ymin) ymin = py;
				if (py > ymax) ymax = py;
			}
		}
	}
	obstacle.x = xmin;
	obstacle.y = ymin;
	obstacle.theta = atan2(xmin, ymin);

	return obs;
}

bool Control::new_goal(const geometry_msgs::Point& target,
  geometry_msgs::Point& prev_target)
{
  //std::cout << "target:" << target.x << ", " << target.y << std::endl;
  //std::cout << "prev_t:" << prev_target.x << ", " << prev_target.y << std::endl;
  if (fabs(target.x - prev_target.x)>0.1
    || fabs(target.y - prev_target.y)>0.1)
  {
    prev_target.x = target.x;
    prev_target.y = target.y;
    std::cout << "NEW GOAL" << std::endl;
    return true;
  }
  return false;
}

void Control::stop(geometry_msgs::Twist& cmd) {
  //std::cout << "stop" << std::endl;
  cmd.linear.x=0;
	cmd.linear.y=0;
	cmd.angular.x=0;
	cmd.angular.y=0;
	cmd.angular.z=0;
}

bool Control::is_arrived(const geometry_msgs::Pose2D& pose,
  const geometry_msgs::Point& target)
{
	double xPos = pose.x - target.x;
	double yPos = pose.y - target.y;
	bool is_arrived = false;
  if (sqrt( pow(xPos,2) + pow(yPos,2) ) < 1.1) //0.2
		is_arrived = true;
	return is_arrived;
}

void Control::updateMove(geometry_msgs::Twist& cmd,
  const geometry_msgs::Pose2D& pose,
  const osmosis_control_msgs::Goal& goal,
  const sensor_msgs::LaserScan& scan,
  const double obstacle_distance,
  const double nu, const double psi, const double safety_distance)
{
  geometry_msgs::Pose2D obstacle;
	// if no obstacle_ or taxi mode on -> avoid
	if(!obstacleFromScan(scan, obstacle, obstacle_distance) || goal.taxi)
	{
		double xPos = pose.x - goal.point.x;
		double yPos = pose.y - goal.point.y;
		double wPos = pose.theta;
		if ( wPos < 0) wPos += 2 * M_PI;
		double obs_dist = sqrt(pow(obstacle.x,2) + pow(obstacle.y,2));
		double obsG_x = (xPos) + obstacle_distance*cos(wPos + obstacle.theta);
		double obsG_y = (yPos) + obstacle_distance*sin(wPos + obstacle.theta);

		//ROS_INFO("Ob_X: %f",obsG_x);
    //ROS_INFO("Ob_Y: %f",obsG_y);
		cmd = PF(xPos,yPos,wPos,obsG_x,obsG_y,nu,psi,safety_distance);
    //std::cout << "command: " << cmd.linear.x << "/" << cmd.angular.z << std::endl;
	}
  else
	 stop(cmd);
}

std::string Control::get_state_str() const {
  if (state == State::WAIT_GOAL) return "WAIT_GOAL";
  if (state == State::MOVE_TO_GOAL) return "MOVE_TO_GOAL";
  if (state == State::ARRIVED_GOAL) return "ARRIVED_GOAL";
}

Control::State Control::get_state() const {
  return state;
}

void Control::step(const geometry_msgs::Pose2D& pose,
  const osmosis_control_msgs::Goal& target,
  const sensor_msgs::LaserScan& scan,
  double obstacle_distance, double nu, double psi, double safety_distance)
{
  switch (state) {
    case State::WAIT_GOAL:
      stop(command);
      if (new_goal(target.point, this->target))
        state = State::MOVE_TO_GOAL;
      break;
    case State::MOVE_TO_GOAL:
      if (new_goal(target.point, this->target))
        state = State::MOVE_TO_GOAL;
      else if (is_arrived(pose, target.point))
        state = State::ARRIVED_GOAL;
      else
        updateMove(command, pose, target, scan,
          obstacle_distance, nu, psi, safety_distance);
      break;
    case State::ARRIVED_GOAL:
      stop(command);
      state = State::WAIT_GOAL;
      break;
    default:
      break;
  }
}

};
