#include <osmosis_control/safetyPilot.hpp>

namespace osmosis {

void SafetyPilot::updateCmdWithLaserScan(geometry_msgs::Twist& cmd,
	const sensor_msgs::LaserScan& s, double stop_lateral_distance,
	double stop_distance, double max_linear, double max_angular)
{

	double obs_dist = std::numeric_limits<double>::max();
	double obs_lat = std::numeric_limits<double>::max();

	for (int i = 0; i < s.ranges.size(); i++)
	{
		double r = s.ranges[i];
		if (r > s.range_min)
		{
			double a = s.angle_min + (s.angle_max - s.angle_min) * i / s.ranges.size();
			double dy = fabs( r * sin(a) );
			if (dy <= stop_lateral_distance)
			{
				obs_dist = std::min(obs_dist, r);
				obs_lat = std::min(obs_lat, dy);
			}
		}
	}

	if (obs_dist < stop_distance)
	{
		//std::cout << "Obstacle distance: " << obs_dist;
		cmd.linear.x = std::min(0.0, cmd.linear.x);
	}

	//std::cout << "input command: " << cmd.linear.x << " / " << cmd.angular.z << std::endl;
	cmd.linear.x = std::min(cmd.linear.x, + max_linear);
	cmd.linear.x = std::max(cmd.linear.x, - max_linear);
	cmd.angular.z = std::min(cmd.angular.z, + max_angular);
	cmd.angular.z = std::max(cmd.angular.z, - max_angular);
	//std::cout << "output command: " << cmd.linear.x << " / " << cmd.angular.z << std::endl;
}

void SafetyPilot::computeCommandTeleop(const osmosis_control_msgs::Teleop& teleop_cmd,
	const geometry_msgs::Twist& ctrl_cmd,
	geometry_msgs::Twist& cmd, const sensor_msgs::LaserScan& s, double stop_lateral_distance,
	double stop_distance, double max_linear, double max_angular, bool fault)
{
	// if user activate teleop
	if (teleop_cmd.is_active)
	{
		cmd.linear.x = teleop_cmd.cmd_vel.linear.x;
		cmd.angular.z = teleop_cmd.cmd_vel.angular.z;
	}
	// else read the command from the control loop (from OsmosisControl)
	else {
		cmd.linear.x = ctrl_cmd.linear.x;
		cmd.angular.z = ctrl_cmd.angular.z;
	}

	//check if we need to override the command due to safe stop from laserScan
	updateCmdWithLaserScan(cmd, s, stop_lateral_distance, stop_distance, max_linear, max_angular);

	if (fault) {
		cmd.linear.x = 1.1*max_linear;
		cmd.angular.z = 1.1*max_angular;
	}
}

SafetyPilot::SafetyPilot() {
	state = State::COMPUTE_CMD;
}

void SafetyPilot::stop(geometry_msgs::Twist& cmd) {
	cmd.linear.x = 0.0;
	cmd.linear.y = 0.0;
	cmd.linear.z = 0.0;
	cmd.angular.x = 0.0;
	cmd.angular.y = 0.0;
	cmd.angular.z = 0.0;
}

std::string SafetyPilot::get_state() const {
	if (state == State::COMPUTE_CMD)
		return "COMPUTE_CMD";
	else if (state == State::TELEOP)
		return "TELEOP";
	else if (state == State::CONTROLLED_STOP)
		return "CONTROLLED_STOP";
}

void SafetyPilot::step(const osmosis_control_msgs::Teleop& teleop_cmd,
	const geometry_msgs::Twist& ctrl_cmd, geometry_msgs::Twist& cmd,
	const sensor_msgs::LaserScan& s, double stop_lateral_distance,
	double stop_distance, double max_linear, double max_angular,
	bool controlled_stop, bool switch_teleop, bool fault)
{
	switch(state) {
		case COMPUTE_CMD:
			if (controlled_stop) {
				stop(cmd);
				state = State::CONTROLLED_STOP;
			}
			else if (switch_teleop) {
				stop(cmd);
				state = State::TELEOP;
			}
			else
				computeCommandTeleop(teleop_cmd, ctrl_cmd, cmd, s,
					stop_lateral_distance, stop_distance, max_linear, max_angular, fault);
			break;
		case CONTROLLED_STOP:
			if (! controlled_stop)
				state = State::COMPUTE_CMD;
			break;
		case TELEOP:
			if (controlled_stop) {
				stop(cmd);
				state = State::CONTROLLED_STOP;
			}
			else if (switch_teleop) {
				osmosis_control_msgs::Teleop active_cmd = teleop_cmd;
				active_cmd.is_active = true;
				computeCommandTeleop(active_cmd, ctrl_cmd, cmd, s,
					stop_lateral_distance, stop_distance, max_linear, max_angular, fault);
			}
			else
				state = COMPUTE_CMD;
			break;
		default:
			break;
	}
}

}; // ns
