#include <osmosis_control/graphPlanner.hpp>

namespace osmosis {

bool GraphPlanner::new_goal(bool& _new_goal)
{
	bool newg=false;
	if (_new_goal)
	{
		newg=true;
		_new_goal=false; // reset for next time
	}
	return newg;
}

void GraphPlanner::compute_plan(Graph& graph,
	const geometry_msgs::Point& current,
	const geometry_msgs::Point& goal,
	std::vector<geometry_msgs::Point>& plan)
{
	auto s = graph.getClosestNode(current);
	auto g = graph.getClosestNode(goal);
	auto p = graph.compute_plan(s, g);
	plan.clear();
	plan.push_back(s->point);
	for (int i = 0; i < p.size(); i++)
		plan.push_back(p[i]->point);

	target_index = 0;
}

bool GraphPlanner::plan_done(const std::vector<geometry_msgs::Point>& plan) {
	return ( target_index >= (int)plan.size() );
}

bool GraphPlanner::is_arrived(bool& target_reached)
{
	if (target_reached) {
		target_reached = false;
		return true;
	}
	return false;
}

bool GraphPlanner::plan_computed(const std::vector<geometry_msgs::Point>& plan)
{
	return plan.size() > 0;
}

void GraphPlanner::step(Graph& graph,
	const geometry_msgs::Point& current,
	const geometry_msgs::Point& goal,
	std::vector<geometry_msgs::Point>& plan,
	bool& new_goal, bool& target_reached)
{
	switch (state_)	{
		case WAIT_GOAL:
			if (this->new_goal(new_goal))	{
				compute_plan(graph, current, goal, plan);
				state_ = WAIT_COMPUTE_PLAN;
			}
			break;

		case WAIT_COMPUTE_PLAN:
			if (plan_computed(plan)) {
				state_ = TEST_NEXT;
			}
			break;

		case TEST_NEXT:
			target_index++; // needed to test plan_done()
			if (plan_done(plan)) {
				state_ = GOAL_DONE;
			}
			else
				state_ = SEND_NEXT;
			break;

		case SEND_NEXT:
			state_ = FOLLOW;

		case FOLLOW:
			if (is_arrived(target_reached))
				state_ = TEST_NEXT;
			break;

		case GOAL_DONE:
			state_ = WAIT_GOAL;
			break;

		default: break;
	}
}

GraphPlanner::GraphPlanner()
{
	state_=WAIT_GOAL;
}

std::string GraphPlanner::get_state_str() const {
	switch (state_) {
	case WAIT_GOAL: return "WAIT_GOAL";
	case WAIT_COMPUTE_PLAN: return "WAIT_COMPUTE_PLAN";
	case TEST_NEXT: return "TEST_NEXT";
	case SEND_NEXT: return "SEND_NEXT";
	case FOLLOW: return "FOLLOW";
	case GOAL_DONE: return "GOAL_DONE";
	}
}

GraphPlanner::State GraphPlanner::get_state() const {
	return state_;
}

// this method should be placed somewhere else... where ???
void GraphPlanner::initGraph(const std::string& filename, Graph& g)
{
	//std::string s = folder + '/' + filename;
	g=parse(filename); // grapIO.hpp method
}

}; // ns
