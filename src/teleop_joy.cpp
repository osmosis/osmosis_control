#include <osmosis_control/teleop_joy.hpp>

namespace osmosis {

TeleopJoy::TeleopJoy() {
	state = State::DESACTIVATED;
}

TeleopJoy::State TeleopJoy::joy_desactivated(const sensor_msgs::Joy& cmd,
	const sensor_msgs::Joy& prev_cmd,
	osmosis_control_msgs::Teleop& teleop)
{
	if (prev_cmd.buttons[7]==0 && cmd.buttons[7]==1) // button pressed
	{
		return State::ACTIVATED;
	}
	else {
		teleop.is_active = false;
		return State::DESACTIVATED;
	}
}

TeleopJoy::State TeleopJoy::joy_activated(const sensor_msgs::Joy& cmd,
	const sensor_msgs::Joy& prev_cmd,
	osmosis_control_msgs::Teleop& teleop)
{
	if (prev_cmd.buttons[7]==0 && cmd.buttons[7]==1) // button pressed
	{
		return State::DESACTIVATED;
	}
	else {
		teleop.is_active = true;
		teleop.cmd_vel.linear.x = cmd.axes[1];
		teleop.cmd_vel.angular.z = cmd.axes[3];
		return State::ACTIVATED;
	}
}

void TeleopJoy::step(const sensor_msgs::Joy& cmd, osmosis_control_msgs::Teleop& teleop) {
	if (state == State::DESACTIVATED) {
		state = joy_desactivated(cmd, prev_cmd, teleop);
	}
	else if (state == State::ACTIVATED) {
		state = joy_activated(cmd, prev_cmd, teleop);
	}
	prev_cmd = cmd;
}

std::string TeleopJoy::get_state() const {
	if (state == State::DESACTIVATED)
		return "DESACTIVATED";
	else if (state == State::ACTIVATED)
		return "ACTIVATED";
}

};
