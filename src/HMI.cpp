//juil2018 J.Guiochet @ LAAS
//juil2019 Anthony FAVIER / Antonin MESSIOUX @ LAAS
#include <osmosis_control/HMI.hpp>

namespace osmosis {

char HMI::askMode()
{
	char mode;
	std::string input;
	printf("Enter the mode : ('P':Point 'M':mission)");
	std::cin >> input;
	mode=input[0];
	return mode;
}

void HMI::goalKeyboard(osmosis_control_msgs::Mission& mission)
{
	geometry_msgs::Point thegoal;
	int n=0;
	mission.doRunwayMission=false;
	printf("Enter a new goal (x,y)");
	printf("x= ");
	std::cin >> thegoal.x;
	printf("y= ");
	std::cin >> thegoal.y;
	printf("taxi (0,1)= ");
	std::cin >> n;
	mission.mission_goal.taxi = n!=0;
	mission.mission_goal.point = thegoal;
}

std::string HMI::askMission()
{
	std::string name;
	printf("Enter the mission : ");
	std::cin >> name;
	return name;
}

void HMI::step(osmosis_control_msgs::Mission& mission, bool& mission_done, std::string folder)
{
	switch (state_)
	{
		case State::IDLE:
			char mode;
			mode=askMode();
			if(mode=='P'||mode=='p')
				state_ = State::REACH_POINT_MISSION;
			else if(mode=='M'||mode=='m')
				state_ = State::RUNWAY_MISSION;
			else
				printf("Input Error : Please try again.\n");
			break;

		case State::REACH_POINT_MISSION:
			switch (mission_state_)
			{
				case StateMission::ASK_MISSION:
					goalKeyboard(mission);
					mission_state_ = StateMission::START_MISSION;
					break;

				case StateMission::START_MISSION:
					mission_done = false;
					mission_state_ = StateMission::WAIT_END_MISSION;
					break;

				case StateMission::WAIT_END_MISSION:
					if (mission_done)
					{
						state_ = State::IDLE;
						mission_state_ = StateMission::ASK_MISSION;
					}
					break;
			}
			break;

		case State::RUNWAY_MISSION:
			switch (mission_state_)
			{
				case StateMission::ASK_MISSION: {
					std::string name = askMission();
					if(loadMission(folder, name, mission)) {
						mission_state_ = StateMission::START_MISSION;
					}
					else {
						state_=State::IDLE;
					}
					break;
				}
				case StateMission::START_MISSION:
					mission_done = false;
					mission_state_=StateMission::WAIT_END_MISSION;
					break;

				case StateMission::WAIT_END_MISSION:
					if(mission_done)
					{
						mission_state_ = StateMission::ASK_MISSION;
						state_ = State::IDLE;
					}
					break;
			}
			break;

		default: break;
	}
}

bool HMI::loadMission(std::string folder, std::string name, osmosis_control_msgs::Mission& mission)
{
	mission.doRunwayMission=true;
	bool ok=false;

	std::string filename = folder + "/MISSION_" + name + ".miss";
	std::ifstream fichier(filename, std::ios::in);
	if(fichier)	{
		ok=true;
		mission.mission_name=name;
		fichier.close();
	}
	return ok;
}

HMI::HMI()
{
	state_=IDLE;
	mission_state_=ASK_MISSION;
}

HMI::State HMI::get_state() const {
	return state_;
}
HMI::StateMission HMI::get_state_mission() const {
	return mission_state_;
}
std::string HMI::get_state_str() const {
	std::string state, mstate;
	switch (state_) {
		case State::IDLE: state = "IDLE";
		case State::REACH_POINT_MISSION: state = "REACH_POINT_MISSION";
		case State::RUNWAY_MISSION: state = "RUNWAY_MISSION";
	}
	switch (mission_state_) {
		case StateMission::ASK_MISSION: mstate = "ASK_MISSION";
		case StateMission::START_MISSION: mstate = "START_MISSION";
		case StateMission::WAIT_END_MISSION: mstate = "WAIT_END_MISSION";
	}
	return state + "/" + mstate;
}

}
