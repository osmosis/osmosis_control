#include <osmosis_control/teleop.hpp>

namespace osmosis {

Teleop::Teleop() {
	state = State::DESACTIVATED;
}

Teleop::State Teleop::keyboard_desactivated(char cmd, osmosis_control_msgs::Teleop& teleop) {
	teleop.cmd_vel = geometry_msgs::Twist();
	if (cmd=='1')
	{
		teleop.is_active=true;
		return State::ACTIVATED;
	}
	else
		return State::DESACTIVATED;
}

Teleop::State Teleop::keyboard_activated(char cmd, osmosis_control_msgs::Teleop& teleop) {
	teleop.cmd_vel = geometry_msgs::Twist();
	//move forward
	if (cmd=='+') {
		//std::cout << "forward" << std::endl;
		teleop.cmd_vel.linear.x = 0.25;
	}
	//turn left (yaw) and drive forward at the same time
	else if(cmd=='l')
	{
		//std::cout << "left" << std::endl;
		teleop.cmd_vel.angular.z = 0.75;
		teleop.cmd_vel.linear.x = 0.25;
	}
	//turn right (yaw) and drive forward at the same time
	else if(cmd=='r')
	{
		//std::cout << "right" << std::endl;
		teleop.cmd_vel.angular.z = -0.75;
		teleop.cmd_vel.linear.x = 0.25;
	}
	//quit
	else if(cmd=='.')
	{
		//std::cout << "desactivate" << std::endl;
		teleop.is_active = false;
		return State::DESACTIVATED;
	}
	return State::ACTIVATED;
}

void Teleop::step(const std::string& cmd, osmosis_control_msgs::Teleop& teleop) {
	//std::cout << "keyboard command: " << cmd[0] << cmd.size() << std::endl;
	if (state == State::DESACTIVATED) {
		state = keyboard_desactivated(cmd[0], teleop);
	}
	else { //if (state == State::ACTIVATED) {
		state = keyboard_activated(cmd[0], teleop);
	}
}

std::string Teleop::get_state() const {
	if (state == State::DESACTIVATED)
		return "DESACTIVATED";
	else if (state == State::ACTIVATED)
		return "ACTIVATED";
}

};
