/*
 * Copyright 2019 LAAS-CNRS
 * MESSIOUX Antonin / FAVIER Anthony
 *
 * This file is part of the OSMOSIS project.
 *
 * Osmosis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Osmosis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
#ifndef OSMOSIS_TELEOP_HPP
#define OSMOSIS_TELEOP_HPP

#include <iostream>
#include <osmosis_control_msgs/Teleop.h>

namespace osmosis {

	class Teleop {
	public:
		enum State {
			DESACTIVATED,
			ACTIVATED
		};
		void step(const std::string& cmd, osmosis_control_msgs::Teleop& teleop);
		Teleop();

		std::string get_state() const;

	private:
		State state;
		State keyboard_desactivated(char cmd, osmosis_control_msgs::Teleop& teleop);
		State keyboard_activated(char cmd, osmosis_control_msgs::Teleop& teleop);
	};

};

#endif
