/*
* Copyright 2018 LAAS-CNRS
*
* This file is part of the OSMOSIS project.
*
* Osmosis is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Osmosis is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
*/

#ifndef OSMOSIS_CONTROL_HPP
#define OSMOSIS_CONTROL_HPP

#include <iostream>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose2D.h>
#include <osmosis_control_msgs/Goal.h>

namespace osmosis {

class Control {
public:
  enum State {
    WAIT_GOAL,
    MOVE_TO_GOAL,
    ARRIVED_GOAL
  };

  Control();

  void step(const geometry_msgs::Pose2D& pose,
    const osmosis_control_msgs::Goal& target,
    const sensor_msgs::LaserScan& scan,
    double obstacle_distance, double nu, double psi, double safety_distance);

  bool new_goal(const geometry_msgs::Point& target,
    geometry_msgs::Point& prev_target);

  void stop(geometry_msgs::Twist& cmd);

  bool is_arrived(const geometry_msgs::Pose2D& pose,
    const geometry_msgs::Point& target);

  void updateMove(geometry_msgs::Twist& cmd,
    const geometry_msgs::Pose2D& pose,
    const osmosis_control_msgs::Goal& goal,
    const sensor_msgs::LaserScan& scan,
    const double obstacle_distance,
    const double nu, const double psi, const double safety_distance);

  bool obstacleFromScan(const sensor_msgs::LaserScan& scan,
    geometry_msgs::Pose2D& obstacle,
    const double obstacle_distance);
  geometry_msgs::Twist PF(double x_p, double y_p,	double theta_p, double obs_dx, double obs_dy,
    const double nu, const double psi, const double safety_distance);

  std::string get_state_str() const;
  State get_state() const;

private:
  State state;
  geometry_msgs::Point target;
protected:
  geometry_msgs::Twist command;
};

};

#endif
