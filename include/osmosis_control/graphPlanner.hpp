/*
* Copyright 2018 LAAS-CNRS
*
* This file is part of the OSMOSIS project.
*
* Osmosis is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Osmosis is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
*/

#ifndef OSMOSIS_GRAPHPLANNER_HPP
#define OSMOSIS_GRAPHPLANNER_HPP


#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Pose2D.h>
#include <std_msgs/Bool.h>
#include <osmosis_control_msgs/Goal.h>

#include <osmosis_control/graph.hpp>

namespace osmosis {

class GraphPlanner
{
public:
	enum State {
		WAIT_GOAL,
		WAIT_COMPUTE_PLAN,
		TEST_NEXT,
		SEND_NEXT,
		FOLLOW,
		GOAL_DONE
	};

	GraphPlanner();

	void step(Graph& graph,
		const geometry_msgs::Point& current,
		const geometry_msgs::Point& goal,
		std::vector<geometry_msgs::Point>& plan,
		bool& new_goal, bool& target_reached);

	bool new_goal(bool& _new_goal);
	bool plan_done(const std::vector<geometry_msgs::Point>& plan);
	bool plan_computed(const std::vector<geometry_msgs::Point>& plan);

	void compute_plan(Graph& graph,
		const geometry_msgs::Point& current,
		const geometry_msgs::Point& goal,
		std::vector<geometry_msgs::Point>& plan);
	bool is_arrived(bool& target_reached);

	void initGraph(const std::string& filename, Graph& g);

	std::string get_state_str() const;
	State get_state() const;

protected:
	int target_index;

private:
	State state_;

}; // end of class

}; // ns

#endif
