/*
 * Copyright 2019 LAAS-CNRS
 * MESSIOUX Antonin / FAVIER Anthony
 *
 * This file is part of the OSMOSIS project.
 *
 * Osmosis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Osmosis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#ifndef OSMOSIS_HMI_HPP
#define OSMOSIS_HMI_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <std_msgs/Bool.h>
#include <osmosis_control_msgs/Goal.h>
#include <osmosis_control_msgs/Mission.h>

namespace osmosis {

class HMI
{
public:
	HMI();

	enum State {
		IDLE,
		REACH_POINT_MISSION,
		RUNWAY_MISSION
	};
	enum StateMission {
		ASK_MISSION,
		START_MISSION,
		WAIT_END_MISSION
	};

	void step(osmosis_control_msgs::Mission& mission, bool& mission_done, std::string folder);

	char askMode();
	void goalKeyboard(osmosis_control_msgs::Mission& mission);
	std::string askMission();
	bool loadMission(std::string folder, std::string name, osmosis_control_msgs::Mission& mission);

	State get_state() const;
	StateMission get_state_mission() const;
	std::string get_state_str() const;

private:
	State state_;
	StateMission mission_state_;
};

}; // ns

#endif
