/*
 * Copyright 2018 LAAS-CNRS
 *
 * This file is part of the OSMOSIS project.
 *
 * Osmosis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Osmosis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#ifndef OSMOSIS_SAFETYPILOT_HPP
#define OSMOSIS_SAFETYPILOT_HPP

#include <iostream>
#include <cmath>

#include <osmosis_control_msgs/Teleop.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <std_msgs/Bool.h>

namespace osmosis {

	class SafetyPilot {
	public:
		enum State {
			COMPUTE_CMD,
			CONTROLLED_STOP,
			TELEOP
		};
		void step(const osmosis_control_msgs::Teleop& teleop_cmd,
			const geometry_msgs::Twist& ctrl_cmd, geometry_msgs::Twist& cmd,
			const sensor_msgs::LaserScan& s, double stop_lateral_distance,
			double stop_distance, double max_linear, double max_angular,
			bool controlled_stop, bool switch_teleop, bool fault);
		SafetyPilot();
		std::string get_state() const;

	private:
		State state;

		void computeCommandTeleop(const osmosis_control_msgs::Teleop& teleop_cmd,
			const geometry_msgs::Twist& ctrl_cmd,
			geometry_msgs::Twist& cmd, const sensor_msgs::LaserScan& s, double stop_lateral_distance,
			double stop_distance, double max_linear, double max_angular, bool fault=false);

		void updateCmdWithLaserScan(geometry_msgs::Twist& cmd,
			const sensor_msgs::LaserScan& s, double stop_lateral_distance,
			double stop_distance, double max_linear, double max_angular);

		void stop(geometry_msgs::Twist& cmd);

	};

};

#endif
