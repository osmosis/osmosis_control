/*
 * Copyright 2019 LAAS-CNRS
 * MESSIOUX Antonin / FAVIER Anthony
 *
 * This file is part of the OSMOSIS project.
 *
 * Osmosis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Osmosis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#ifndef OSMOSIS_MISSION_MANAGER_HPP
#define OSMOSIS_MISSION_MANAGER_HPP

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <std_msgs/Bool.h>
#include <osmosis_control_msgs/Mission.h>
#include <osmosis_control_msgs/Goal.h>

namespace osmosis {

  class MissionManager {
  public:
    enum State {
      IDLE,
      REACH_POINT_MISSION,
      RUNWAY_MISSION,
      MISSION_DONE
    };
  	enum StateTask {
      INIT_MISSION,
      SEND_MISSION,
      EXECUTE_MISSION
    };

    MissionManager();

    void initMission(std::string folder, std::string name);

    void step(bool& mission_received,
      bool& goal_reached,
      const osmosis_control_msgs::Mission mission,
      std_msgs::Bool& done_msg,
      osmosis_control_msgs::Goal& goal_msg,
      std::string mission_folder);

    std::string get_state_str() const;
    State get_state() const;
    StateTask get_state_task() const;

    void goalKeyboard(const osmosis_control_msgs::Mission& mission,
      osmosis_control_msgs::Goal& goal);
    void setDone(std_msgs::Bool& done);
    bool isMissionOver();
    void nextStep(bool& goal_reached, osmosis_control_msgs::Goal& goal);
    bool checkNextStep(bool& goal_reached, osmosis_control_msgs::Goal& goal);

  private:
    StateTask mission_state;
    State state;

    void parse(std::string line);

  protected:
    std::vector<osmosis_control_msgs::Goal> steps;
    int mission_step;
  };

};

#endif
